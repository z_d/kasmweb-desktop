FROM kasmweb/core-ubuntu-bionic:1.10.0
USER root

ENV HOME /home/kasm-default-profile
ENV STARTUPDIR /dockerstartup
ENV INST_SCRIPTS $STARTUPDIR/install
WORKDIR $HOME


######### Timezone
ENV TZ=Europe/Moscow
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone


######### Customize Container Here ###########

RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y apt-transport-https \
    && apt-get install -y python3-pip dnsutils iputils-ping traceroute telnet mc putty firefox remmina geany chrony htop tor wireguard openvpn unzip python3.8 software-properties-common \
    && cp /usr/share/applications/geany.desktop $HOME/Desktop/ \
    && chmod +x $HOME/Desktop/geany.desktop  \
    && cp /usr/share/applications/firefox.desktop $HOME/Desktop/ \
    && chmod +x $HOME/Desktop/firefox.desktop \
    && cp /usr/share/applications/org.remmina.Remmina.desktop $HOME/Desktop/ \
    && chmod +x $HOME/Desktop/org.remmina.Remmina.desktop \
    && cp /usr/share/applications/putty.desktop $HOME/Desktop/ \
    && chmod +x $HOME/Desktop/putty.desktop \
    && chown 1000:1000 $HOME/Desktop/geany.desktop \
    && chown 1000:1000 $HOME/Desktop/putty.desktop \
    && chown 1000:1000 $HOME/Desktop/org.remmina.Remmina.desktop \
    && chown 1000:1000 $HOME/Desktop/firefox.desktop \
    && apt-get install -y sudo \
    && useradd -m -d /home/kasm-user -s /bin/bash kasm-user \
    && echo 'kasm-user ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers \
    && rm -rf /var/lib/apt/list/* \
    && apt-get autoremove -y

####### Install Sublime-text

RUN  wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | apt-key add - \
    && apt-get update \
    && echo "deb https://download.sublimetext.com/ apt/stable/" |  tee /etc/apt/sources.list.d/sublime-text.list \
    && apt-get update \
    && apt-get install sublime-text \
    && cp /usr/share/applications/sublime_text.desktop $HOME/Desktop/ \
    && chmod +x $HOME/Desktop/sublime_text.desktop \
    && chown 1000:1000 $HOME/Desktop/sublime_text.desktop


######### Install Nextcloud
#
COPY ./install/nextcloud $INST_SCRIPTS/nextcloud/
RUN bash $INST_SCRIPTS/nextcloud/install_nextcloud.sh && rm -rf $INST_SCRIPTS/nextcloud/


######### Install VS Code

COPY ./install/vs_code $INST_SCRIPTS/vs_code/
RUN bash $INST_SCRIPTS/vs_code/install_vs_code.sh  && rm -rf $INST_SCRIPTS/vs_code/

######### Install Ansible

RUN pip3 install --upgrade pip; \
    python3 -m pip install ansible; \
    python3 -m pip install paramiko


######### Python 3.8 setup default

RUN update-alternatives --install /usr/bin/python python /usr/bin/python3.8 10


######### End Customizations ###########


RUN chown 1000:0 $HOME
RUN $STARTUPDIR/set_user_permission.sh $HOME

ENV HOME /home/kasm-user
WORKDIR $HOME
RUN mkdir -p $HOME && chown -R 1000:0 $HOME

USER 1000

